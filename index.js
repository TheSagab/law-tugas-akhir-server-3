const express = require('express');
const fileUpload = require('express-fileupload');
const bodyParser = require('body-parser');
const app = express();
const path = require('path');
const amqp = require('amqplib/callback_api');
const fs = require("fs");
const JSZip = require("jszip");
const saveAs = require('file-saver');
const { file } = require('jszip');
const router = express.Router();
const port = process.env.port || 3001

app.use(bodyParser.json());         // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
    extended: true
})); 

app.use(fileUpload({
    limits: { fileSize: 50 * 1024 * 1024 },
}));

router.post('/',function(req, resp){
    const files = req.files.files;
    console.log(files);
    const zip = new JSZip();
    files.forEach(file => {
        const fileName = file.name;
        const data = file.data;
        zip.file(fileName, data);
    });
    zip.generateNodeStream({type:'nodebuffer', streamFiles:true})
        .pipe(fs.createWriteStream('out.zip'))
        .on('finish', function () {
            // JSZip generates a readable stream with a "end" event,
            // but is piped here in a writable stream which emits a "finish" event.
            console.log("out.zip written.");
        });
    
    // console.log(req.body)
    // amqp.connect('amqp://0806444524:0806444524@152.118.148.95:5672/%2f0806444524', function(error0, connection) {
    //     if (error0) {
    //         throw error0;
    //     }
    //     connection.createChannel(function(error1, channel) {
    //         if (error1) {
    //             throw error1;
    //         }
    //         var exchange = '1606879230_DIRECT';
    //         var routingKey = "SagabRoutingKey";
    //         channel.assertExchange(exchange, 'direct', {
    //             durable: false
    //         });
    //         var msg = JSON.stringify(req.body)
    //         channel.publish(exchange, routingKey, Buffer.from(msg));
    //         console.log(" [x] Sent %s: '%s'", routingKey, msg);
    //         setTimeout(function() {
    //             connection.close();
    //         },  500);
    //     });
    // });
    resp.send("ok");
});

async function zipFile(fileName, data, routingKey) {
    amqp.connect('amqp://0806444524:0806444524@152.118.148.95:5672/%2f0806444524', function(error0, connection) {
        if (error0) {
            throw error0;
        }
        connection.createChannel(function(error1, channel) {
            if (error1) {
                throw error1;
            }
            var exchange = '1606879230';
            var msg = "Starting zipping file..."
            channel.assertExchange(exchange, 'direct', {
                durable: false
            });
            channel.publish(exchange, routingKey, Buffer.from(msg))
    
            const zip = new JSZip();
            zip.file(fileName, data);
            // ... and other manipulations
            // zip
            zip.generateNodeStream({type:'nodebuffer', streamFiles:true})
            .pipe(fs.createWriteStream('out.zip'))
            .on('finish', function () {
                // JSZip generates a readable stream with a "end" event,
                // but is piped here in a writable stream which emits a "finish" event.
                console.log("out.zip written.");
                var msg = "Zipping file completed"
                channel.publish(exchange, routingKey, Buffer.from(msg))
            });
            setTimeout(function() {
                connection.close();
            },  500);
        });
    });
}


app.use('/', router);
app.listen(port);

console.log(`Example app listening on port ${port}!`);
